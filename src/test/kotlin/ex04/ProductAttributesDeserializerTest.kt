package ex04

import com.fasterxml.jackson.databind.DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES
import com.fasterxml.jackson.module.kotlin.jacksonObjectMapper
import com.fasterxml.jackson.module.kotlin.readValue
import org.junit.jupiter.api.Test
import strikt.api.expectCatching
import strikt.assertions.isA
import strikt.assertions.isEqualTo
import strikt.assertions.isSuccess

internal class ProductAttributesDeserializerTest {

    private val mapper = jacksonObjectMapper()
        .registerModule(ProductAttributesDeserializer.module)
        .disable(FAIL_ON_UNKNOWN_PROPERTIES)

    @Test
    fun `should deserialize Product successfully`() {
        val jsonString = """
            {
                "id": "4686dda6-c3c6-4d0f-8ab2-279234aa079b",
                "name": "LEGO Harry Potter Hogwarts Castle",
                "attributes": [
                    { "name": "maxQuantityPerOrder", "value": 3 },
                    { "name": "weight", "value": "5kg" },
                    { "name": "dimensions",
                      "value": [
                          { "name": "height", "value": "50cm" },
                          { "name": "width", "value": "40cm" },
                          { "name": "depth", "value": "30cm" } ]
                    }
                ]
            }
        """.trimIndent()

        expectCatching { mapper.readValue<Product>(jsonString) }.isSuccess().and {
            get { attributes }.isA<ProductAttributes>().and {
                get { maxQuantityPerOrder } isEqualTo 3
                get { weight } isEqualTo "5kg"
                get { dimensions } isEqualTo Dimensions("50cm", "40cm", "30cm")
            }
        }
    }

}
