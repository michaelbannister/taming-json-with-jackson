package ex04

import java.util.*

data class Product(
    val id: UUID,
    val name: String,
    val attributes: ProductAttributes,
)

data class ProductAttributes(
    val weight: String,
    val maxQuantityPerOrder: Int,
    val dimensions: Dimensions,
)

data class Dimensions(
    val height: String,
    val width: String,
    val depth: String,
)