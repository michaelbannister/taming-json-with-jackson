package ex04

import com.fasterxml.jackson.core.JsonParser
import com.fasterxml.jackson.databind.*
import com.fasterxml.jackson.databind.deser.BeanDeserializerModifier
import com.fasterxml.jackson.databind.deser.std.DelegatingDeserializer
import com.fasterxml.jackson.databind.module.SimpleModule
import com.fasterxml.jackson.databind.node.ArrayNode
import com.fasterxml.jackson.databind.node.JsonNodeFactory
import com.fasterxml.jackson.databind.node.ObjectNode
import com.fasterxml.jackson.databind.node.TreeTraversingParser

class ProductAttributesDeserializer(defaultDeserializer: JsonDeserializer<*>) :
   DelegatingDeserializer(defaultDeserializer) {
   override fun newDelegatingInstance(newDelegatee: JsonDeserializer<*>): JsonDeserializer<*> {
      return ProductAttributesDeserializer(newDelegatee)
   }

   override fun deserialize(p: JsonParser, ctxt: DeserializationContext): Any {
      return super.deserialize(restructure(p), ctxt)
   }

   private fun restructure(parser: JsonParser): JsonParser {
      val product = parser.readValueAsTree<ObjectNode>()
      val attributesArray = product.get("attributes") as ArrayNode
      val attributesObject = reshapeAttributes(attributesArray)
      product.replace("attributes", attributesObject)
      val newJsonParser = TreeTraversingParser(product, parser.codec)
      newJsonParser.nextToken()
      return newJsonParser
   }

   companion object {
      // This is a convenient place to create a single instance of the Module
      // which we need to register with the ObjectMapper.

      val module = object : SimpleModule() {}.apply {
         setDeserializerModifier(object : BeanDeserializerModifier() {
            override fun modifyDeserializer(
               config: DeserializationConfig,
               beanDesc: BeanDescription,
               deserializer: JsonDeserializer<*>
            ): JsonDeserializer<*> {
               return if (Product::class.java.isAssignableFrom(beanDesc.beanClass)) {
                  ProductAttributesDeserializer(deserializer)
               } else deserializer
            }
         })
      }
   }
}

internal fun reshapeAttributes(attributesArray: ArrayNode): ObjectNode {
   val newObject = ObjectNode(JsonNodeFactory.instance)
   attributesArray.forEach {
      val (name, value) = reshapeSingleAttribute(it as ObjectNode)
      newObject.set<JsonNode>(name, value)
   }
   return newObject
}

private fun reshapeSingleAttribute(attribute: ObjectNode): Pair<String, JsonNode> {
   val name = attribute.get("name").asText()
   val value = attribute.get("value")

   return if (value.isArray && value.get(0).isObject) {
      name to reshapeAttributes(value as ArrayNode)
   } else {
      name to value
   }
}
