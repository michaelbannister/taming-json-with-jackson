@file:Suppress("MemberVisibilityCanBePrivate")

package ex03

import com.fasterxml.jackson.databind.JsonNode
import com.fasterxml.jackson.databind.node.ArrayNode
import com.fasterxml.jackson.databind.node.JsonNodeFactory
import com.fasterxml.jackson.databind.node.ObjectNode
import com.fasterxml.jackson.module.kotlin.jacksonObjectMapper
import com.fasterxml.jackson.module.kotlin.readValue
import org.junit.jupiter.api.Test
import strikt.api.expectThat
import strikt.assertions.isEqualTo

class JsonNodeRestructureTest {
    val attributesArrayJson = """
        [
           { "name": "stockItemId", "value": "a4b3c2d1" },
           { "name": "weight", "value": "5kg" },
           { "name": "maxQuantityPerOrder", "value": 3 },
           {
              "name": "dimensions",
              "value": [
                 { "name": "height", "value": "50cm" },
                 { "name": "width", "value": "40cm" },
                 { "name": "depth", "value": "30cm" }
              ]
           }
        ]
    """.trimIndent()

    val attributesObjectJson = """
        {
           "stockItemId": "a4b3c2d1",
           "weight": "5kg",
           "maxQuantityPerOrder": 3,
           "dimensions": {
              "height": "50cm",
              "width": "40cm",
              "depth": "30cm"
           }
        }
    """.trimIndent()

    @Test
    fun `restructure array of key-value attributes into object`() {
        val objectMapper = jacksonObjectMapper()

        val attributesArrayNode = objectMapper.readValue<ArrayNode>(attributesArrayJson)

        val attributesObjectNode = reshapeAttributes(attributesArrayNode)

        expectThat(objectMapper.readValue<JsonNode>(attributesObjectJson)) isEqualTo attributesObjectNode
    }

    internal fun reshapeAttributes(attributesArray: ArrayNode): ObjectNode {
        val newObject = ObjectNode(JsonNodeFactory.instance)
        attributesArray.forEach {
            val (name, value) = reshapeSingleAttribute(it as ObjectNode)
            newObject.set<JsonNode>(name, value)
        }
        return newObject
    }

    private fun reshapeSingleAttribute(attribute: ObjectNode): Pair<String, JsonNode> {
        val name = attribute.get("name").asText()
        val value = attribute.get("value")

        return if (value.isArray && value.get(0).isObject) {
            name to reshapeAttributes(value as ArrayNode)
        } else {
            name to value
        }
    }
}