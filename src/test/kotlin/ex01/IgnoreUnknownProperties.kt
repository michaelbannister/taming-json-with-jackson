@file:Suppress("MemberVisibilityCanBePrivate", "SpellCheckingInspection")

package ex01

import com.fasterxml.jackson.databind.DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES
import com.fasterxml.jackson.databind.exc.UnrecognizedPropertyException
import com.fasterxml.jackson.module.kotlin.jacksonObjectMapper
import com.fasterxml.jackson.module.kotlin.readValue
import org.junit.jupiter.api.Test
import strikt.api.expectCatching
import strikt.api.expectThrows
import strikt.assertions.isSuccess
import java.util.*

class IgnoreUnknownProperties {
    data class Product(
        val id: UUID,
        val name: String,
    )

    val extendedProductJson = """
        {
            "id": "4686dda6-c3c6-4d0f-8ab2-279234aa079b",
            "name": "LEGO 71043 Harry Potter Hogwarts Castle",
            "price": { "amount": 349.99, "currency": "GBP" }
        }
    """.trimIndent()

    @Test
    fun `by default the mapper will fail if the JSON contains properties that are not in the target class`() {
        val objectMapper = jacksonObjectMapper()

        expectThrows<UnrecognizedPropertyException> {
            objectMapper.readValue<Product>(extendedProductJson)
        }
    }

    @Test
    fun `you should almost always disable the FAIL_ON_UNKNOWN_PROPERTIES deserialization feature`() {
        // This is known as the Robustness principle or Postel's law
        // https://en.wikipedia.org/wiki/Robustness_principle

        val objectMapper = jacksonObjectMapper().disable(FAIL_ON_UNKNOWN_PROPERTIES)

        expectCatching {
            objectMapper.readValue<Product>(extendedProductJson)
        }.isSuccess()
    }
}