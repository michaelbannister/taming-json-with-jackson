@file:Suppress("MemberVisibilityCanBePrivate", "SpellCheckingInspection")

package ex01

import com.fasterxml.jackson.databind.json.JsonMapper
import com.fasterxml.jackson.module.kotlin.KotlinModule
import com.fasterxml.jackson.module.kotlin.jacksonObjectMapper
import com.fasterxml.jackson.module.kotlin.readValue
import org.junit.jupiter.api.Test
import strikt.api.expectThat
import strikt.assertions.isEqualTo
import java.math.BigDecimal
import java.util.*

class SimpleDatabindTest {

    //region very simple product
    data class VerySimpleProduct(
        val id: UUID,
        val name: String,
    )

    val verySimpleProductJson = """
        {
            "id": "4686dda6-c3c6-4d0f-8ab2-279234aa079b",
            "name": "LEGO 71043 Harry Potter Hogwarts Castle"
        }
    """.trimIndent()

    @Test
    fun `deserialize very simple product`() {
        val objectMapper = JsonMapper().registerModule(KotlinModule())

        val deserialisedProduct = objectMapper.readValue(verySimpleProductJson, VerySimpleProduct::class.java)

        expectThat(deserialisedProduct) {
            get { id } isEqualTo UUID.fromString("4686dda6-c3c6-4d0f-8ab2-279234aa079b")
            get { name } isEqualTo "LEGO 71043 Harry Potter Hogwarts Castle"
        }
    }
    //endregion

    //region simple product
    data class SimpleProduct(
        val id: UUID,
        val name: String,
        val price: Money,
    )

    data class Money(
        val amount: BigDecimal,
        val currency: Currency,
    )

    val simpleProductJson = """
        {
            "id": "4686dda6-c3c6-4d0f-8ab2-279234aa079b",
            "name": "LEGO 71043 Harry Potter Hogwarts Castle",
            "price": { "amount": 349.99, "currency": "GBP" }
        }
    """.trimIndent()

    @Test
    fun `deserialize simple product`() {
        val objectMapper = jacksonObjectMapper()

        val deserialisedProduct = objectMapper.readValue<SimpleProduct>(simpleProductJson)

        expectThat(deserialisedProduct) {
            get { price } isEqualTo Money(BigDecimal("349.99"), Currency.getInstance("GBP"))
        }
    }
    //endregion
}